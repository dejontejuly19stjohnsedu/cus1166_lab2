import static org.junit.Assert.assertEquals;

import org.junit.Test;
public class GFGTest {
	
	// this creates a new object of type GFG called GFGtest1
	GFG GFGtest1 = new GFG();
	
	//displayBlance 
	//amountWithdrawing 
	//amountDepositing 
	
	// testing the display balance method of the GFG class
	@Test
	public void testdisplayBalance() {
		assertEquals(10000, 10000);

	}
	
	//testing the withdraw method of the GFG class
	@Test
	public void testAmountWithdrawing() {
		int amountWithdrawn = GFGtest1.amountWithdrawing(10000, 375);
		assertEquals(9625,amountWithdrawn);
	}
	
	
	//testing the depsoit method of the GFG class
	@Test
	public void testAmountDespositing() {
		int amountDeposited = GFGtest1.amountDepositing(9625, 1500);
		assertEquals(11125,amountDeposited);
	}

}
